$(document).ready(function(){
    $('ul.tabs li a:first').addClass('active');
    $('.perfil_nav article').hide();
    $('.perfil_nav article:first').show();

    $('ul.tabs li a').click(function(){
        $('ul.tabs li a').removeClass('active');
        $(this).addClass('active');
        $('.perfil_nav article').hide();

        var activeTab = $(this).attr('href');
        $(activeTab).show();
        return false;
    });
});