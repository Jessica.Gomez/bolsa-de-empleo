<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-witdh, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilos.css">
    <title>Empleo</title>
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
  </head>
  <body>
    <header>
      <nav class="menu">
        <ul>
          <li id="item"><a href="index.php" class="link">Inicio</a></li>
          <li id="item"><a href="#" class="link">Normatividad</a>
            <ul class="submenu">
              <li><a href="https://www.serviciodeempleo.gov.co/normatividad/leyes">Leyes</a></li>
              <li><a href="https://www.serviciodeempleo.gov.co/normatividad/resoluciones">Resoluciones</a></li>
            </ul>
          </li>
          <li id="item"><a href="#" class="link">Atencion al ciudadano</a>
            <ul class="submenu">
              <li><a href="https://www.serviciodeempleo.gov.co/atencion-al-ciudadano/radique-su-pqrsd">Quejas y reclamos </a></li>
              <li><a href="https://www.serviciodeempleo.gov.co/atencion-al-ciudadano/protocolo-de-atencion-al-ciudadano">Protocolo</a></li>
            </ul>
          </li>
          <li id=item><a href="#" class="link">Contactos</a>
            <ul class="submenu"> 
              <li> <a href="mailto:Jessica.gomez.manrique@unillanos.edu.co">Correo</a></li>
              <li><a href="https://www.facebook.com/jessiiquita.11">Facebook</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </header>
    <main>
      <section class="services">
        <div class="container">
          <article class="container_cards">

            <div class="card">
              <img src="img/buscador_empleo.png" class="card_img">
              <div class="card_text">
                <p class="card_list">¿Busca empleo?</p>
                <p class="card_copy">Registrese o inicie sesion para encontrar o modificar sus solicitudes de empleo al igual que sus datos proporcionados</p>
                <a href="login&signup.php" class="card_button">Ingresar</a>
              </div>
            </div>

            <div class="card">
              <img src="img/empleador.png" class="card_img">
              <div class="card_text">
                <p class="card_list">¿Buscas empleados?</p>
                <p class="card_copy">Entre como empresa para ofrecer empleos a miles de personas capacitadas en busca de dar el mejor desempeño</p>
                <a href="loginemp.php" class="card_button">Ingresar</a>
              </div>
            </div>

            <div class="card">
              <img src="img/preguntas.png" class="card_img">
              <div class="card_text">
                <p class="card_list">Preguntas frecuentes</p>
                <p class="card_copy">Si tienes alguna pregunta sobre como manejar nuestra plataforma tenemos las respuestas para ti</p>
                <a href="#" class="card_button">Ingresar</a>
              </div>
            </div>

          </article>
        </div>
      </section>
    </main>
    <footer class="footer">
      <div class="footer_info">

        <div class="footer_left_img">
          <img src="#" alt="">
        </div>

        <div class="footer_info_text">
          <h2 class="footer_h2">Unidad Administrativa Especial del Servicio Público de Empleo</h2>
          <ul>
             <li class="footer_list">Dirección: Carrera 69 # 25 B - 44 Piso 7, Bogotá D.C.</li>
             <li class="footer_list">PBX: <a href="#">+5717560009 Opción 1.</a></li>
             <li class="footer_list">Correspondencia: Lunes a Viernes 8:00 am a 4:30 pm</li>
          </ul>
        </div>

        <div class="footer_info_text">
          <h2 class="footer_h2">Atención al Ciudadano</h2>
          <ul>
              <li class="footer_list">En Bogotá: 7560009 opción 1.</li>
              <li class="footer_list">Lunes - Viernes de 7:00 am - 5:00 pm.</li>
              <li class="footer_list">Escribenos</li>
          </ul>
        </div>

        <div class="footer_right_img">
          <img src="#" alt="">
        </div>

      </div>
    </footer>
  </body>
</html>
