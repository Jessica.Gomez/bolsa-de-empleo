<?php

include("logic/conexion.php");
session_start();
$nitL = $_SESSION['username'];

if(!isset($nitL)){
  header("location: loginemp.php");
}else{
  echo "<script>alert('Bienvenido ',$nitL);</script>";
}

$demandantes = "SELECT * FROM demandantes where nit = '$nitL' ";


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-witdh, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilos.css">
    <title>Empleo</title>
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="main.js"></script>
  </head>
  <body>
    <header>
      
    </header>
    <main>
        <section class="wrap">
            <ul class="tabs">
                <li class="datos_hojavida"><a href="#tab1">Datos</a></li>
                <li class="datos_hojavida"><a href="#tab2">Ofrecer vacante</a></li>
                <li class="datos_vacante"><a href="#tab3">Vacantes</a></li>
                <li class="datos_buscar"><a href="logic/salir.php">Salir</a></li>
            </ul>
            <div class="perfil_nav">
                <article id="tab1">
                <fieldset>
                  <legend>Datos generales</legend>
                  <ul class="hv_datos">
                    <?php 
                      $resultado = mysqli_query($conexion, $demandantes);
                      while($row=mysqli_fetch_assoc($resultado)){
                    ?>
                    <li>Nit: <?php echo $row["nit"]; ?></li>
                    <li>Verificacion: <?php echo $row["verif"]; ?></li>
                    <li>Nombre: <?php echo $row["nomb"]; ?></li>
                    <li>Telefono: <?php echo $row["cel_dem"]; ?></li>
                    <li>Correo: <?php echo $row["email"]; ?></li>
                    <li>Sector: <?php echo $row["sector"]; ?></li>
                    <?php } mysqli_free_result($resultado);?>
                  </ul>
                </fieldset>
                </article>
                <article id="tab2">
                    <h1>Vacantes</h1>
                </article>
                <article id="tab3">
                    <h1>Vacantes</h1>
                </article>
                <article id="tab4">
                    <h1>Buscar</h1>
                </article>
            </div>
        </section>
    </main>
    <footer class="footer">
      <div class="footer_info">

        <div class="footer_left_img">
          <img src="#" alt="">
        </div>

        <div class="footer_info_text">
          <h2 class="footer_h2">Unidad Administrativa Especial del Servicio Público de Empleo</h2>
          <ul>
             <li class="footer_list">Dirección: Carrera 69 # 25 B - 44 Piso 7, Bogotá D.C.</li>
             <li class="footer_list">PBX: <a href="#">+5717560009 Opción 1.</a></li>
             <li class="footer_list">Correspondencia: Lunes a Viernes 8:00 am a 4:30 pm</li>
          </ul>
        </div>

        <div class="footer_info_text">
          <h2 class="footer_h2">Atención al Ciudadano</h2>
          <ul>
              <li class="footer_list">En Bogotá: 7560009 opción 1.</li>
              <li class="footer_list">Lunes - Viernes de 7:00 am - 5:00 pm.</li>
              <li class="footer_list">Escribenos</li>
          </ul>
        </div>

        <div class="footer_right_img">
          <img src="#" alt="">
        </div>

      </div>
    </footer>
  </body>
</html>