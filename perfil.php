<?php

include("logic/conexion.php");
session_start();
$cedulaL = $_SESSION['username'];

if(!isset($cedulaL)){
  header("location: login&signup.php");
}else{
  echo "<script>alert('Bienvenido ',$cedulaL);</script>";
}

$usuarios = "SELECT * FROM oferente where cedula = '$cedulaL' ";
$estudios = "SELECT * FROM estudio where cedula = '$cedulaL' ";
$referencia = "SELECT * FROM referencias WHERE cedula = '$cedulaL' ";
$exp = "SELECT * FROM exp_laboral WHERE cedula = '$cedulaL' ";


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-witdh, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilos.css">
    <title>Empleo</title>
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="main.js"></script>
  </head>
  <body>
    <header>
      
    </header>
    <main>
        <section class="wrap">
            <ul class="tabs">
                <li class="datos_hojavida"><a href="#tab1">Hoja de vida</a></li>
                <li class="datos_hojavida"><a href="#tab2">Modificar hoja de vida</a></li>
                <li class="datos_vacante"><a href="#tab3">Vacantes</a></li>
                <li class="datos_buscar"><a href="#tab4">Buscar trabajo</a></li>
                <li class="datos_buscar"><a href="logic/salir.php">Salir</a></li>
            </ul>
            <div class="perfil_nav">
                <article id="tab1">
                  <div class="container_hv">
                    <fieldset>
                      <legend>Datos generales</legend>
                      <ul class="hv_datos">
                        <?php 
                          function calculaedad($fechanacimiento){
                            list($ano,$mes,$dia) = explode("-",$fechanacimiento);
                            $ano_diferencia  = date("Y") - $ano;
                            $mes_diferencia = date("m") - $mes;
                            $dia_diferencia   = date("d") - $dia;
                            if ($dia_diferencia < 0 || $mes_diferencia < 0)
                              $ano_diferencia--;
                            return $ano_diferencia;
                          }
                          $resultado = mysqli_query($conexion, $usuarios);
                          while($row=mysqli_fetch_assoc($resultado)){
                        ?>
                        <li>Cedula: <?php echo $row["cedula"]; ?></li>
                        <li>Nombres: <?php echo $row["nom"]; ?></li>
                        <li>Apellidos: <?php echo $row["apelli"]; ?></li>
                        <li>Telefono: <?php echo $row["cel"]; ?></li>
                        <li>Fecha de nacimiento: <?php echo $row["fec_nac"]; ?></li>
                        <li>Edad: <?php echo calculaedad($row["fec_nac"]); ?></li>
                        <li>Correo: <?php echo $row["correo"]; ?></li>
                        <li>Estrato: <?php echo $row["estrato"]; ?></li>
                        <li>Direccion: <?php echo $row["direccion"]; ?></li>
                        <li>Ciudad: <?php echo $row["ciudad"]; ?></li>
                        <?php } mysqli_free_result($resultado);?>
                      </ul>
                    </fieldset>
                    <fieldset>
                      <legend>Estudio</legend>
                      <div class="container_table">
                        <div class="table_header">Titulo</div>
                        <div class="table_header">Fecha de inicio</div>
                        <div class="table_header">Fecha final</div>
                        <div class="table_header">Institucion</div>
                        <div class="table_header">Estado</div>
                        <?php 
                          $resultado2 = mysqli_query($conexion, $estudios);
                          while($row=mysqli_fetch_assoc($resultado2)){
                        ?>
                        <div class="table_item"><?php echo $row["titulo"]; ?></div>
                        <div class="table_item"><?php echo $row["fec_inici"]; ?></div>
                        <div class="table_item"><?php echo $row["fec_final"]; ?></div>
                        <div class="table_item"><?php echo $row["institucion"]; ?></div>
                        <div class="table_item"><?php echo $row["estado"]; ?></div>
                        <?php } mysqli_free_result($resultado2);?>
                      </div>
                    </fieldset>
                    <fieldset>
                      <legend>Referencias</legend>
                      <div class="container_table">
                        <div class="table_header">Tipo de referencia</div>
                        <div class="table_header">Nombre</div>
                        <div class="table_header">Apellido</div>
                        <div class="table_header">Telefono</div>
                        <div class="table_header">Vinculo</div>
                        <?php 
                          $resultado3 = mysqli_query($conexion, $referencia);
                          while($row=mysqli_fetch_assoc($resultado3)){
                        ?>
                        <div class="table_item"><?php echo $row["tipo"]; ?></div>
                        <div class="table_item"><?php echo $row["nombre"]; ?></div>
                        <div class="table_item"><?php echo $row["apellido"]; ?></div>
                        <div class="table_item"><?php echo $row["cel_ref"]; ?></div>
                        <div class="table_item"><?php echo $row["vinculo"]; ?></div>
                        <?php } mysqli_free_result($resultado3);?>
                      </div>
                    </fieldset>
                    <fieldset>
                      <legend>Experencia laboral</legend>
                      <div class="container_table2">
                        <div class="table_header">Puesto</div>
                        <div class="table_header">Empresa</div>
                        <div class="table_header">F. de inicio</div>
                        <div class="table_header">F. de salida</div>
                        <div class="table_header">Tiempo (M)</div>
                        <div class="table_header">Sueldo</div>
                        <div class="table_header">Tel. empresa</div>
                        <div class="table_header">Dir. empresa</div>
                        <?php
                          $resultado4 = mysqli_query($conexion, $exp);
                          while($row=mysqli_fetch_assoc($resultado4)){
                        ?>
                        <div class="table_item"><?php echo $row["nom_puesto"]; ?></div>
                        <div class="table_item"><?php echo $row["nom_emp"]; ?></div>
                        <div class="table_item"><?php echo $row["fec_inic"]; ?></div>
                        <div class="table_item"><?php echo $row["fec_final"]; ?></div>
                        <div class="table_item"><?php echo $row["tiempo_tl"]; ?></div>
                        <div class="table_item"><?php echo $row["sueldo"]; ?></div>
                        <div class="table_item"><?php echo $row["cel_emp"]; ?></div>
                        <div class="table_item"><?php echo $row["dir_emp"]; ?></div>
                        <?php } mysqli_free_result($resultado4);?>
                      </div>
                    </fieldset>
                  </div>
                </article>
                <article id="tab2">
                <div class="container_hv conatiner_hv_edit">
                    <fieldset class="datos_generales">
                      <legend>Datos generales</legend>
                      <div class="container_table_edit">
                      <form action="actualizar.php" method="POST">
                        <?php 
                          $resultado2 = mysqli_query($conexion, $usuarios);
                          while($row=mysqli_fetch_assoc($resultado2)){
                        ?>
                        <div class="table_header_edit">Cedula: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["cedula"]; ?>" name="cedula">
                        <div class="table_header_edit">Nombre: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["nom"]; ?>" name="nom">                     
                        <div class="table_header_edit">Apellidos: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["apelli"]; ?>" name="apelli">
                        <div class="table_header_edit">Telefono: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["cel"]; ?>" name="cel">                      
                        <div class="table_header_edit">Fecha de nacimiento: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["fec_nac"]; ?>" name="fec_nac">
                        <div class="table_header_edit">Correo: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["correo"]; ?>" name="correo">
                        <div class="table_header_edit">Estrato: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["estrato"]; ?>" name="estrato">
                        <div class="table_header_edit">Direccion: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["direccion"]; ?>" name="direccion">
                        <div class="table_header_edit">Ciudad: </div>
                        <input type="text" class="table_item_edit" value="<?php echo $row["ciudad"]; ?>" name="ciudad">
                        <div class="table_header_edit">Clave: </div>
                        <input type="password" class="table_item_edit" value="<?php echo $row["clave"]; ?>" name="clave">
                        <div class="table_header_edit">Operacion: </div>
                        <input type="submit" value="Cambiar">
                        <?php } mysqli_free_result($resultado2);?>
                        </form>
                      </div>
                    </fieldset>
                    <fieldset>
                    <legend>Estudio <a href="nuevo.php?cedula=<?php $cedulaL?>">Nuevo</a></legend>
                      <form class="container_table container_table_edit2" action="actualizar2.php" method="POST">
                        <div class="table_header">Titulo</div>
                        <div class="table_header">Fecha de inicio</div>
                        <div class="table_header">Fecha final</div>
                        <div class="table_header">Institucion</div>
                        <div class="table_header">Estado</div>
                        <div class="table_header">Operacion</div>
                        <?php 
                          $resultado2 = mysqli_query($conexion, $estudios);
                          while($row=mysqli_fetch_assoc($resultado2)){
                        ?>
                        <input type="hidden" class="table_item" value="<?php echo $row["cedula"]; ?>" name="cedula">
                        <input type="text" class="table_input" value="<?php echo $row["titulo"]; ?>" name="titulo">
                        <input type="text" class="table_input" value="<?php echo $row["fec_inici"]; ?>" name="fec_inici">
                        <input type="text" class="table_input" value="<?php echo $row["fec_final"]; ?>" name="fec_final">
                        <input type="text" class="table_input" value="<?php echo $row["institucion"]; ?>" name="institucion">
                        <input type="text" class="table_input" value="<?php echo $row["estado"]; ?>" name="estado">
                        <div class="table_item">
                          <input type="submit" class="table_item_link" value="Editar">
                          <input type="submit" class="table_item_link" value="Eliminar">
                        </div>
                        <?php } mysqli_free_result($resultado2);?>
                      </form>
                    </fieldset>
                    <fieldset>
                    <legend>Referencias <a href="">Nuevo</a></legend>
                    <form class="container_table container_table_edit2" action="actualizar3.php" method="POST">
                        <div class="table_header">Tipo de referencia</div>
                        <div class="table_header">Nombre</div>
                        <div class="table_header">Apellido</div>
                        <div class="table_header">Telefono</div>
                        <div class="table_header">Vinculo</div>
                        <div class="table_header">Operaciones</div>
                        <?php 
                          $resultado3 = mysqli_query($conexion, $referencia);
                          while($row=mysqli_fetch_assoc($resultado3)){
                        ?>
                        <input type="hidden" class="table_item" value="<?php echo $row["cedula"]; ?>" name="cedula">
                        <input type="text" class="table_item" value="<?php echo $row["tipo"]; ?>" name="tipo">
                        <input type="text" class="table_input" value="<?php echo $row["nombre"]; ?>" name="nombre">
                        <input type="text" class="table_input" value="<?php echo $row["apellido"]; ?>" name="apellido">
                        <input type="text" class="table_input" value="<?php echo $row["cel_ref"]; ?>" name="cel_ref">
                        <input type="text" class="table_input" value="<?php echo $row["vinculo"]; ?>" name="vinculo">
                        <div class="table_item">
                          <input type="submit" class="table_item_link" value="Editar">
                          <input type="submit" class="table_item_link" value="Eliminar">
                        </div>
                        <?php } mysqli_free_result($resultado3);?>
                      </form>
                    </fieldset>
                    <fieldset>
                      <legend>Experencia laboral <a href="">Nuevo</a></legend>
                      <form class="container_table_edit3" action="actualizar4.php" method="POST">
                        <div class="table_header">Puesto</div>
                        <div class="table_header">Empresa</div>
                        <div class="table_header">F. de inicio</div>
                        <div class="table_header">F. de salida</div>
                        <div class="table_header">Tiempo (M)</div>
                        <div class="table_header">Sueldo</div>
                        <div class="table_header">Tel. empresa</div>
                        <div class="table_header">Dir. empresa</div>
                        <div class="table_header">Operaciones</div>
                        <?php
                          $resultado4 = mysqli_query($conexion, $exp);
                          while($row=mysqli_fetch_assoc($resultado4)){
                        ?>
                        <input type="hidden" class="table_input" value="<?php echo $row["cedula"]; ?>" name="cedula">
                        <input type="text" class="table_input" value="<?php echo $row["nom_puesto"]; ?>" name="nom_puesto">
                        <input type="text" class="table_input" value="<?php echo $row["nom_emp"]; ?>" name="nom_esp">
                        <input type="text" class="table_input" value="<?php echo $row["fec_inic"]; ?>" name="fec_inic">
                        <input type="text" class="table_input" value="<?php echo $row["fec_final"]; ?>" name="fec_final">
                        <input type="text" class="table_input" value="<?php echo $row["tiempo_tl"]; ?>" name="tiepo_tl">
                        <input type="text" class="table_input" value="<?php echo $row["sueldo"]; ?>" name="sueldo">
                        <input type="text" class="table_input" value="<?php echo $row["cel_emp"]; ?>" name="cel_emp">
                        <input type="text" class="table_input" value="<?php echo $row["dir_emp"]; ?>" name="dir_emp">
                        <div class="table_item">
                          <input type="submit" class="table_item_link" value="Editar">
                          <input type="submit" class="table_item_link" value="Eliminar">
                        </div>
                        <?php } mysqli_free_result($resultado4);?>
                          </form>
                    </fieldset>
                  </div>
                </article>
                <article id="tab3">
                    <h1>Vacantes</h1>
                </article>
                <article id="tab4">
                    <h1>Buscar</h1>
                </article>
            </div>
        </section>
    </main>
    <footer class="footer">
      <div class="footer_info">

        <div class="footer_left_img">
          <img src="#" alt="">
        </div>

        <div class="footer_info_text">
          <h2 class="footer_h2">Unidad Administrativa Especial del Servicio Público de Empleo</h2>
          <ul>
             <li class="footer_list">Dirección: Carrera 69 # 25 B - 44 Piso 7, Bogotá D.C.</li>
             <li class="footer_list">PBX: <a href="#">+5717560009 Opción 1.</a></li>
             <li class="footer_list">Correspondencia: Lunes a Viernes 8:00 am a 4:30 pm</li>
          </ul>
        </div>

        <div class="footer_info_text">
          <h2 class="footer_h2">Atención al Ciudadano</h2>
          <ul>
              <li class="footer_list">En Bogotá: 7560009 opción 1.</li>
              <li class="footer_list">Lunes - Viernes de 7:00 am - 5:00 pm.</li>
              <li class="footer_list">Escribenos</li>
          </ul>
        </div>

        <div class="footer_right_img">
          <img src="#" alt="">
        </div>

      </div>
    </footer>
  </body>
</html>